# Copyright (c) Facebook, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
#
import argparse
from batch_eval_KB_completion import main as run_evaluation
from batch_eval_KB_completion import load_file
from lama.modules import build_model_by_name
import pprint
import statistics
from os import listdir
import os
from os.path import isfile, join
from shutil import copyfile
from collections import defaultdict

LMs = [
    # {
    #     "lm": "transformerxl",
    #     "label": "transformerxl",
    #     "models_names": ["transformerxl"],
    #     "transformerxl_model_name": "transfo-xl-wt103",
    #     "transformerxl_model_dir": "pre-trained_language_models/transformerxl/transfo-xl-wt103/",
    # },
    # {
    #     "lm": "elmo",
    #     "label": "elmo",
    #     "models_names": ["elmo"],
    #     "elmo_model_name": "elmo_2x4096_512_2048cnn_2xhighway",
    #     "elmo_vocab_name": "vocab-2016-09-10.txt",
    #     "elmo_model_dir": "pre-trained_language_models/elmo/original",
    #     "elmo_warm_up_cycles": 10,
    # },
    # {
    #     "lm": "elmo",
    #     "label": "elmo5B",
    #     "models_names": ["elmo"],
    #     "elmo_model_name": "elmo_2x4096_512_2048cnn_2xhighway_5.5B",
    #     "elmo_vocab_name": "vocab-enwiki-news-500000.txt",
    #     "elmo_model_dir": "pre-trained_language_models/elmo/original5.5B/",
    #     "elmo_warm_up_cycles": 10,
    # },
    # {
    #     "lm": "bert",
    #     "label": "bert_base",
    #     "models_names": ["bert"],
    #     "bert_model_name": "bert-base-cased",
    #     "bert_model_dir": "pre-trained_language_models/bert/cased_L-12_H-768_A-12",
    # },
    # {
    #     "lm": "bert",
    #     "label": "bert_large",
    #     "models_names": ["bert"],
    #     "bert_model_name": "bert-large-cased",
    #     "bert_model_dir": "pre-trained_language_models/bert/cased_L-24_H-1024_A-16",
    # },
    # {
    #     "lm": "roberta",
    #     "label": "roberta_large",
    #     "models_names": ["roberta"],
    #     "roberta_model_name": "model.pt",
    #     "roberta_model_dir": "pre-trained_language_models/roberta/roberta.large",
    #     "roberta_vocab_name": "vocab.txt",
    #     "max_sentence_length":999999
    # },

    # {
    #     "lm": "HF_roberta",
    #     "label": "HF_roberta_large",
    #     "models_names": ["HF_roberta"],
    #     "roberta_model_name": "roberta-large",
    #     "roberta_model_dir": "pre-trained_language_models/HF_roberta",
    #      "roberta_vocab_name": "vocab.txt",
    # },
        {
        "lm": "RuleBERT",
        "label": "RuleBERT",
        "models_names": ["RuleBERT"],
        "roberta_model_name": "rulebert",
        "roberta_model_dir": "pre-trained_language_models/rulebert",
         "roberta_vocab_name": "vocab.txt",
    }

]


def run_experiments(
    relations,
    data_path_pre,
    data_path_post,
    input_param={
        "lm": "bert",
        "label": "bert_large",
        "models_names": ["bert"],
        "bert_model_name": "bert-large-cased",
        "bert_model_dir": "pre-trained_language_models/bert/cased_L-24_H-1024_A-16",
    },
    use_negated_probes=True,
):
    model = None
    pp = pprint.PrettyPrinter(width=41, compact=True)

    all_Precision1 = []
    all_spearman1=[]
    all_overlap1=[]
    type_Precision1 = defaultdict(list)
    type_spearman1=defaultdict(list)
    type_overlap1=defaultdict(list)
    type_count = defaultdict(list)

    results_file = open("last_results.csv", "w+")

    for relation in relations:
        pp.pprint(relation)
        PARAMETERS = {
            "dataset_filename": "{}{}{}".format(
                data_path_pre, relation["relation"], data_path_post
            ),
            "common_vocab_filename": "pre-trained_language_models/common_vocab_cased.txt",
            "template": "",
            "bert_vocab_name": "vocab.txt",
            "batch_size": 32,
            "logdir": "output",
            "full_logdir": "output/results/{}/{}".format(
                input_param["label"], relation["relation"]
            ),
            "lowercase": False,
            "max_sentence_length": 100,
            "threads": -1,
            "interactive": False,
            "use_negated_probes": use_negated_probes,
        }

        if "template" in relation:
            PARAMETERS["template"] = relation["template"]
            if use_negated_probes:
                PARAMETERS["template_negated"] = relation["template_negated"]

        PARAMETERS.update(input_param)
        print(PARAMETERS)

        args = argparse.Namespace(**PARAMETERS)

        # see if file exists
        try:
            data = load_file(args.dataset_filename)
        except Exception as e:
            print("Relation {} excluded.".format(relation["relation"]))
            print("Exception: {}".format(e))
            continue

        if model is None:
            [model_type_name] = args.models_names
            model = build_model_by_name(model_type_name, args)

        if(args.use_negated_probes):
            Precision1,spearman1,overlap1 = run_evaluation(args, shuffle_data=False, model=model)
        else:
            Precision1 = run_evaluation(args, shuffle_data=False, model=model)

        print("P@1 : {}".format(Precision1), flush=True)
        all_Precision1.append(Precision1)
        if(args.use_negated_probes):
          all_spearman1.append(spearman1)
          all_overlap1.append(overlap1)

        results_file.write(
            "{},{}\n".format(relation["relation"], round(Precision1 * 100, 2))
        )
        results_file.flush()

        if "type" in relation:
            type_Precision1[relation["type"]].append(Precision1)

            if(args.use_negated_probes):
              type_spearman1[relation['type']].append(spearman1)
              type_overlap1[relation['type']].append(overlap1)

            data = load_file(PARAMETERS["dataset_filename"])
            type_count[relation["type"]].append(len(data))

    mean_p1 = statistics.mean(all_Precision1)
    print("@@@ {} - mean P@1: {}".format(input_param["label"], mean_p1))
    if(args.use_negated_probes):
        mean_spearman=statistics.mean(all_spearman1)
        print("@@@ {} - mean Spearman: {}".format(input_param["label"], mean_spearman))
        #print(f'ALL SPEARMAN:{all_spearman1}')
        #print(f'ALL OVERLAP:{all_overlap1}')
        mean_overlap=statistics.mean(all_overlap1)
        print("@@@ {} - mean OverLap: {}".format(input_param["label"], mean_overlap))
    results_file.close()

    for t, l in type_Precision1.items():

        print(
            "@@@ Precision ",
            input_param["label"],
            t,
            statistics.mean(l),
            sum(type_count[t]),
            len(type_count[t]),
            flush=True,
        )

    if(args.use_negated_probes):
        for t, l in type_spearman1.items():
            print(
            "@@@ Spearman ",
            input_param["label"],
            t,
            statistics.mean(l),
            sum(type_count[t]),
            len(type_count[t]),
            flush=True,
        )


        for t, l in type_overlap1.items():
            print(
            "@@@ Overlap ",
            input_param["label"],
            t,
            statistics.mean(l),
            sum(type_count[t]),
            len(type_count[t]),
            flush=True,
        )


    return mean_p1, all_Precision1


def get_TREx_parameters(data_path_pre="data/"):
    relations = load_file("{}relations.jsonl".format(data_path_pre))
    data_path_pre += "TREx/"
    data_path_post = ".jsonl"
    return relations, data_path_pre, data_path_post


def get_GoogleRE_parameters():
    relations = [
        {
            "relation": f"place_of_birth_test_{i}",
            "template": "[X] was born in [Y] .",
            "template_negated": "[X] was not born in [Y] .",
        } for i in range(6)]+[
        {
            "relation": f"date_of_birth_test_{i}",
            "template": "[X] (born [Y]).",
            "template_negated": "[X] (not born [Y]).",
        } for i in range(4)]+[
        {
            "relation": f"place_of_death_test_{i}",
            "template": "[X] died in [Y] .",
            "template_negated": "[X] did not die in [Y] .",
        } for i in range(4)
    ]

    relations = [
        {
            "relation": "place_of_birth",
            "template": "[X] was born in [Y] .",
            "template_negated": "[X] was not born in [Y] .",
        },
        {
            "relation": "date_of_birth",
            "template": "[X] (born [Y]).",
            "template_negated": "[X] (not born [Y]).",
        },
        {
            "relation": "place_of_death",
            "template": "[X] died in [Y] .",
            "template_negated": "[X] did not die in [Y] .",
        },
    ]
    data_path_pre = "data/Google_RE/"
    data_path_post = "_test.jsonl"
    #data_path_post = ".jsonl"
    return relations, data_path_pre, data_path_post


def get_ConceptNet_parameters(data_path_pre="data/"):
    relations = [{"relation": f"test{i}"} for i in range(11)]
    #relations = [{"relation": "test"}]
    data_path_pre += "ConceptNet/"
    data_path_post = ".jsonl"
    return relations, data_path_pre, data_path_post


def get_Squad_parameters(data_path_pre="data/"):
    relations = [{"relation": "test"}]
    data_path_pre += "Squad/"
    data_path_post = ".jsonl"
    return relations, data_path_pre, data_path_post


def run_all_LMs(parameters):
    for ip in LMs:
        print(ip["label"])
        run_experiments(*parameters, input_param=ip, use_negated_probes=True)


if __name__ == "__main__":

    print("1. Google-RE")
    parameters = get_GoogleRE_parameters()
    run_all_LMs(parameters)

    # print("2. T-REx")
    # parameters = get_TREx_parameters()
    # run_all_LMs(parameters)

    # print("3. ConceptNet")
    # parameters = get_ConceptNet_parameters()
    # run_all_LMs(parameters)

    # print("4. SQuAD")
    # parameters = get_Squad_parameters()
    # run_all_LMs(parameters)


